﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using BottomNavigationBar;
using BottomNavigationBar.Listeners;
using Xamarin.Andorid.Utils;
using Xamarin.Andorid.Fragments.Home;
using Android.Graphics;
using Android.Views;

namespace Xamarin.Andorid
{
    [Activity(Label = "Xamarin.Andorid", MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/AppTheme")]
    public class MainActivity : AppCompatActivity, IOnMenuTabClickListener
    {
        private BottomBar _bottomBar;

        public void OnMenuTabReSelected(int menuItemId)
        {

        }

        public void OnMenuTabSelected(int menuItemId)
        {
            Fragment _content = null;
            switch ((NavigationBarTabs)menuItemId) {
                case NavigationBarTabs.Home:
                    _content = new HomeFragment();
                    break;
                case NavigationBarTabs.Services:
                    _content = new ServicesFragment();
                    break;
                case NavigationBarTabs.Departments:
                    _content = new DepartmentsFragment();
                    break;
                case NavigationBarTabs.News:
                    _content = new NewsFragment();
                    break;
            }
            if (_content != null)
            {
                FragmentManager.BeginTransaction().Replace(Resource.Id.tab_content, _content).Commit();
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            }

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);


            _bottomBar = BottomBar.Attach(this, bundle);
            _bottomBar.SetItems(new[] {
                new BottomBarTab(Resource.Drawable.cool_icon, "Home") { Id = (int)NavigationBarTabs.Home },
                new BottomBarTab(Resource.Drawable.dead_icon, "Services") { Id = (int)NavigationBarTabs.Services },
                new BottomBarTab(Resource.Drawable.excited_icon, "Departments") { Id = (int)NavigationBarTabs.Departments },
                new BottomBarTab(Resource.Drawable.smile_icon, "News") { Id = (int)NavigationBarTabs.News },
            });
            _bottomBar.SetOnMenuTabClickListener(this);

        }
    }
}

