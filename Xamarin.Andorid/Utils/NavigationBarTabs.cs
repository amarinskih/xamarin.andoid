﻿namespace Xamarin.Andorid.Utils
{
    public enum NavigationBarTabs
    {
        Home, Services, Departments, News, Contacts
    }
}